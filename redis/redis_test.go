package redis

import (
	"log"
	"os"
	"testing"
)

var (
	words     []string = []string{"abcd", "abc", "abgl", "我的小胖猫", "我的vim配置"}
	RedisAddr          = os.Getenv("REDIS_ADDR")
	pool               = NewTrie(RedisAddr)
	root      int64
)

func TestDo(t *testing.T) {
	conn := NewConn(RedisAddr)
	reply, err := conn.Do("PING")
	log.Println(reply, err)
	reply, err = conn.Do("SET", "root", 1)
	log.Println(reply, err)
	reply, err = conn.Do("GET", "root")
	log.Println(reply, err)
	conn.Close()
}

func TestPipe(t *testing.T) {
	conn := NewConn(RedisAddr)
	cmds := []struct {
		c []interface{}
	}{
		{
			[]interface{}{"INCR", "mycounter"},
		},
		{
			[]interface{}{"INCR", "mycounter"},
		},
		{
			[]interface{}{"INCR", "mycounter"},
		},
	}
	for _, cmd := range cmds {
		conn.Send(cmd.c[0].(string), cmd.c[1:])
	}
	conn.Flush()

	for range cmds {
		reply, err := conn.Receive()
		log.Println(reply, err)
	}
	conn.Close()
}

func TestPool(t *testing.T) {
	pool := NewTrie(RedisAddr)
	cmds := []struct {
		c []interface{}
	}{
		{
			[]interface{}{"PING"},
		},
		{
			[]interface{}{"GET", "hehe"},
		},
		{
			[]interface{}{"INCR", "mycounter"},
		},
		{
			[]interface{}{"INCR", "mycounter"},
		},
		{
			[]interface{}{"INCR", "mycounter"},
		},
	}
	for _, cmd := range cmds {
		log.Println("active count", pool.ActiveCount(), pool.IdleCount())
		conn := pool.Get()
		reply, err := conn.Do(cmd.c[0].(string), cmd.c[1:])
		log.Println(reply, err)
		conn.Close()
	}
}

func TestFindBySuffix(t *testing.T) {
	var cases = []struct {
		input    string
		expected map[string]struct{}
	}{
		{"ab", map[string]struct{}{"abcd": struct{}{}, "abc": struct{}{}, "abgl": struct{}{}}},
		{"abc", map[string]struct{}{"abcd": struct{}{}, "abc": struct{}{}}},
		{"abgl", map[string]struct{}{"abgl": struct{}{}}},
		{"我", map[string]struct{}{"我的小胖猫": struct{}{}, "我的vim配置": struct{}{}}},
		{"我的", map[string]struct{}{"我的小胖猫": struct{}{}, "我的vim配置": struct{}{}}},
		{"abd", map[string]struct{}{}},
		{"acd", map[string]struct{}{}},
	}

	for _, c := range cases {
		r, err := pool.FindBySuffix(c.input)
		if err != nil {
			t.Fatalf("FindBySuffix return err %v", err)
		}
		t.Log(c.input, r)
		if len(c.expected) == 0 && len(r) > 0 {
			t.Fatalf("FindBySuffix(%s) = %v, must be %v", c.input, r, c.expected)
		}
		for _, item := range r {
			if _, ok := c.expected[item.Word]; !ok {
				t.Fatalf("FindBySuffix(%s) = %v, must be %v", c.input, r, c.expected)
				break
			}
		}
	}
}

func TestTranversal(t *testing.T) {
	check := make(map[string]bool)
	for _, w := range words {
		check[w] = true
	}

	r := pool.Tranversal(root)
	for _, item := range r {
		t.Log(item.Word)
		if _, ok := check[item.Word]; !ok {
			t.Fatalf("Tranversal() = %v, must be %v", r, words)
			break
		}
	}
}

func TestDelete(t *testing.T) {
	pool.DeleteAll()
	result := pool.Tranversal(root)
	t.Log(result)
	if len(result) != 0 {
		t.Fatalf("TestDelete fail,traversal result:%v", result)
	}
}

func TestNilRoot(t *testing.T) {
	err := pool.DeleteAll()
	if err == nil {
		t.Fatalf("TestNilRoot DeleteAll fail err")
	}
	result, err := pool.FindBySuffix("ab")
	if err == nil {
		t.Fatalf("TestNilRoot FindBySuffix fail err")
	}
	result = pool.Tranversal(root)
	t.Log(result)
	if len(result) != 0 {
		t.Fatalf("TestNilRoot fail,traversal result:%v", result)
	}
}

func TestMain(m *testing.M) {
	for _, w := range words {
		root = pool.Generate(root, w)
	}
	println("root", root)

	r := m.Run()
	os.Exit(r)
}
