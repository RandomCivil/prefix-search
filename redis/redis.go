package redis

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"sync/atomic"
	"time"

	R "github.com/gomodule/redigo/redis"
)

var (
	keyId int64
)

func NewConn(addr string) R.Conn {
	c, err := R.Dial("tcp", addr)
	if err != nil {
		// handle error
	}
	return c
}

type Trie struct {
	*R.Pool
}

func NewTrie(addr string) *Trie {
	rp := &R.Pool{
		MaxIdle:     3,
		IdleTimeout: 60 * time.Second,
		// Dial or DialContext must be set. When both are set, DialContext takes precedence over Dial.
		Dial: func() (R.Conn, error) { return R.Dial("tcp", addr) },
	}
	return &Trie{Pool: rp}
}

func (t *Trie) FindBySuffix(suffix string) ([]TrieResult, error) {
	conn := t.Pool.Get()
	defer conn.Close()

	root, err := R.Int64(conn.Do("GET", "root"))
	if err == R.ErrNil {
		return nil, errors.New("not root found")
	}

	result, cur, rs := []TrieResult{}, root, []rune(suffix)
	for i := 0; i < len(rs); i++ {
		rsf := fmt.Sprintf("%c", rs[i])
		next, err := R.Int64(conn.Do("HGET", cur, rsf))
		if err == R.ErrNil {
			return []TrieResult{}, nil
		}
		cur = next
	}
	for _, v := range t.Tranversal(cur) {
		result = append(result, TrieResult{Word: suffix + v.Word})
	}
	return result, nil
}

func (t *Trie) Tranversal(root int64) []TrieResult {
	conn := t.Pool.Get()
	defer conn.Close()

	result := []TrieResult{}
	var (
		b   []byte
		dfs func(head int64)
	)
	dfs = func(head int64) {
		resp, _ := R.ByteSlices(conn.Do("HGETALL", head))
		i := 0
		for i < len(resp) {
			key := resp[i]
			val, _ := strconv.Atoi(string(resp[i+1]))
			//key=="end"
			if bytes.Compare(key, []byte{101, 110, 100}) == 0 {
				if val == 1 {
					result = append(result, TrieResult{Word: string(b)})
					i += 2
				}
			} else {
				t := b
				b = append(b, key...)
				dfs(int64(val))
				b = t
			}
			i += 2
		}
	}
	dfs(root)
	return result
}

func (t *Trie) DeleteAll() error {
	conn := t.Pool.Get()
	defer conn.Close()

	root, err := R.Int64(conn.Do("GET", "root"))
	if err == R.ErrNil {
		return errors.New("not root found")
	}

	var dfs func(head int64)
	dfs = func(head int64) {
		keys := []string{}
		i := 0
		resp, _ := R.ByteSlices(conn.Do("HGETALL", head))
		for i < len(resp) {
			key := resp[i]
			val, _ := strconv.Atoi(string(resp[i+1]))
			//key=="end"
			if bytes.Compare(key, []byte{101, 110, 100}) != 0 {
				keys = append(keys, string(key))
				dfs(int64(val))
			}
			i += 2
		}

		for _, d := range keys {
			conn.Send("HDEL", head, d)
		}
	}
	dfs(root)
	conn.Flush()
	conn.Do("DEL", "root")
	return nil
}

func (t *Trie) Generate(root int64, word string) int64 {
	conn := t.Pool.Get()
	defer conn.Close()

	var cur int64
	if root == 0 {
		kid := atomic.AddInt64(&keyId, 1)
		conn.Do("HSET", kid, "end", 0)
		conn.Do("SET", "root", kid)
		root, cur = kid, kid
	} else {
		cur = root
	}

	rs := []rune(word)
	for i := 0; i < len(rs); i++ {
		rsf := fmt.Sprintf("%c", rs[i])
		reply, err := R.Int64(conn.Do("HGET", cur, rsf))
		if err == R.ErrNil {
			kid := atomic.AddInt64(&keyId, 1)
			conn.Do("HSET", cur, rsf, kid)
			cur = kid
		} else {
			cur = reply
		}
	}
	conn.Do("HSET", cur, "end", 1)
	return root
}

type TrieResult struct {
	Word string
}
