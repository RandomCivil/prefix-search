FROM golang:1.14.9-alpine3.12

WORKDIR /go/src/prefix-search

RUN apk add --no-cache gcc musl-dev bash curl openssl
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN rm -r ~/.cache/go-build/
