package prefix_search

import (
	"reflect"
	"testing"
)

var tests = []struct {
	input    string
	expected []string
}{
	{"theviper", []string{"t", "th", "the", "thev", "thevi", "thevip", "thevipe", "theviper"}},
	{"tree path sum", []string{"t", "tr", "tre", "tree", "treep", "treepa", "treepat", "treepath", "treepaths", "treepathsu", "treepathsum"}},
	{"我的vim配置", []string{"w", "wo", "wod", "wode", "wodev", "wodevi", "wodevim", "wodevimp", "wodevimpe", "wodevimpei", "wodevimpeiz", "wodevimpeizh", "wodevimpeizhi"}},
}

func TestConvert(t *testing.T) {
	for _, ut := range tests {
		uc := GenPinyinPrefix(ut.input)
		if !reflect.DeepEqual(uc, ut.expected) {
			t.Errorf("Convert(%s) = %s, must be %s", ut.input, uc, ut.expected)
		}
	}
}
