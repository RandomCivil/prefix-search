package prefix_search

import (
	"strings"

	gp "github.com/mozillazg/go-pinyin"
)

func GenPinyinPrefix(str string) []string {
	result := []string{}
	p := gp.NewArgs()
	for i := 0; i < len(str); i++ {
		if str[i] == ' ' {
			continue
		}

		var key string
		if len(string(str[i])) > 1 && len(string(str[i:])) > 1 {
			pinyinStr := gp.Pinyin(string(str[i:i+3]), p)
			key = str[:i] + pinyinStr[0][0]
			str = key + str[i+3:]
		} else {
			key = strings.ToLower(str[:i+1])
		}
		key = strings.Replace(str[:i+1], " ", "", -1)
		result = append(result, key)
	}
	return result
}
