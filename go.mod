module gitlab.com/RandomCivil/prefix-search

go 1.14

require (
	github.com/gomodule/redigo v1.8.2
	github.com/mozillazg/go-pinyin v0.18.0
)
