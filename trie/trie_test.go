package trie

import (
	"os"
	"testing"
)

var (
	root  *Node
	words []string = []string{"abcd", "abc", "abgl", "cgl"}
	trie           = New()
)

func tranversalTest(t *testing.T) {
	check := make(map[string]bool)
	for _, w := range words {
		check[w] = true
	}

	r := trie.Tranversal(nil)
	if len(r) != len(words) {
		t.Fatalf("Tranversal(%v) = %v, must be %v", root, r, words)
		return
	}

	for _, item := range r {
		if _, ok := check[item]; !ok {
			t.Fatalf("Tranversal(%v) = %v, must be %v", root, r, words)
			break
		}
	}
}

func findBySuffixTest(t *testing.T) {
	var cases = []struct {
		input    string
		expected map[string]bool
	}{
		{"ab", map[string]bool{"abcd": true, "abc": true, "abgl": true}},
		{"abc", map[string]bool{"abcd": true, "abc": true}},
		{"abgl", map[string]bool{"abgl": true}},
		{"abd", map[string]bool{}},
		{"acd", map[string]bool{}},
	}

	for _, c := range cases {
		r := trie.FindByPrefix(c.input)
		if len(c.expected) == 0 && len(r) > 0 {
			t.Fatalf("FindBySuffix(%s) = %v, must be %v", c.input, r, c.expected)
		}
		if len(r) != len(c.expected) {
			t.Fatalf("FindBySuffix(%s) = %v, must be %v", c.input, r, c.expected)
			break
		}

		for _, item := range r {
			if _, ok := c.expected[item]; !ok {
				t.Fatalf("FindBySuffix(%s) = %v, must be %v", c.input, r, c.expected)
				break
			}
		}
	}
}

func TestParallel(t *testing.T) {
	t.Run("trie", func(t *testing.T) {
		t.Run("findBySuffix", findBySuffixTest)
		t.Run("tranversal", tranversalTest)
	})
}

func TestNilRoot(t *testing.T) {
	trie.Root = nil
	trie.DeleteAll()
	result := trie.Tranversal(nil)
	if len(result) != 0 {
		t.Fatalf("TestNilRoot fail,traversal result:%v", result)
	}
}

func TestDelete(t *testing.T) {
	trie.DeleteAll()
	result := trie.Tranversal(nil)
	if len(result) != 0 {
		t.Fatalf("TestDelete fail,traversal result:%v", result)
	}
}

func TestMain(m *testing.M) {
	for _, w := range words {
		trie.Generate([]byte(w))
	}
	println("root", trie.Root)

	r := m.Run()
	os.Exit(r)
}
