package trie

type Node struct {
	end  bool
	m    map[byte]*Node
	Root *Node
}

func New() *Node {
	return new(Node)
}

func (t *Node) Generate(bytes []byte) {
	var (
		m    map[byte]*Node
		cur  *Node
		size = len(bytes)
	)
	if t.Root == nil {
		cur = &Node{m: make(map[byte]*Node)}
		t.Root = cur
	} else {
		cur = t.Root
	}

	for i := 0; i < size; i++ {
		m = cur.m
		if val, ok := m[bytes[i]]; ok {
			cur = val
		} else {
			node := &Node{m: make(map[byte]*Node)}
			m[bytes[i]] = node
			cur = node
		}
	}
	cur.end = true
}

func (t *Node) FindByPrefix(prefix string) []string {
	size, cur := len(prefix), t.Root
	var m map[byte]*Node

	for i := 0; i < size; i++ {
		m = cur.m
		if val, ok := m[prefix[i]]; ok {
			cur = val
		} else {
			return []string{}
		}
	}
	result := []string{}
	for _, v := range t.Tranversal(cur) {
		result = append(result, prefix+v)
	}
	return result
}

func (t *Node) Tranversal(start *Node) []string {
	if start == nil {
		start = t.Root
	}
	result := []string{}
	var dfs func(head *Node, str string)
	dfs = func(head *Node, str string) {
		if head == nil {
			return
		}
		if head.end {
			result = append(result, str)
		}
		for char, next := range head.m {
			dfs(next, str+string(char))
		}
	}
	dfs(start, "")
	return result
}

func (t *Node) DeleteAll() {
	var dfs func(head *Node, str string)
	dfs = func(head *Node, str string) {
		if head == nil {
			return
		}
		for char, next := range head.m {
			dfs(next, str+string(char))
		}
		head.m = nil
		head.end = false
	}
	dfs(t.Root, "")
}
